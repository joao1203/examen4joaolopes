import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val totalE = mutableListOf<Int>()
    val sentences = mutableListOf<String>()
    do{
        println("Escriu una frase:\r")
        val phrase = scanner.nextLine()
        var count = 0
        sentences.add(phrase)

        for(i in phrase){
            if(i == 'e'){
                count++
            }
        }
        totalE.add(count)
        var largest = totalE[0]

        for (num in totalE) {
            if (largest < num)
                largest = num
        }
        var big = totalE.indexOf(largest)
        println("La frase amb més 'e' és: \"${sentences[big]}\"")
        println("Té $largest 'e'")

    }while(phrase != "fi")
}